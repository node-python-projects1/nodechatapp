var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app)
var io = require('socket.io')(http)
var mongoose = require('mongoose');
var port = process.env.PORT || 5000
var dbUrl ='mongodb+srv://Monica:apjabdulkalam@cluster0.pkuzv.mongodb.net/mongo-node?retryWrites=true&w=majority'
var Message = mongoose.model('Message',{
    name: String,
    message: String
})
var messages=[]

app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

app.get('/messages',(req,res)=>{
    Message.find({}, (err,messages)=>{
        res.send(messages)
    })
   
})

app.post('/messages',(req,res)=>{
    var message = new Message(req.body)
    message.save((err)=>{
        if(err){
            sendStatus(500)
        }
        messages.push(req.body);
        io.emit('message',req.body)
    })
   
})

io.on('connection',(socket)=>{
    console.log('user connected')
})

mongoose.connect(dbUrl, (err)=> {
    console.log('mongodb connection successful')
  })

var server = http.listen(port,()=>{
    console.log('app is listening at port', port);
});
// server.address().port
